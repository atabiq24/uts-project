package org.app.uts.npm189;

import java.util.List;
import org.app.uts.npm189.model.Buku;
import org.app.uts.npm189.repository.BukuRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ProjectUts43A87006180189Application {

	public static void main(String[] args) {
		SpringApplication.run(ProjectUts43A87006180189Application.class, args);
	}
        
        @Bean
        public CommandLineRunner testBukuRepository(BukuRepository repo){
            return a -> {  
                List<Buku> list = repo.findAll();
                System.out.format("%-15s %-40s %-30s %-20s %-10s \n", "IdBuku", "Judul", "Pengarang",
                            "Penerbit", "Tahun");
                    System.out.println("-----------------------------------------------------------------"
                            + "--------------------------------------------------------------------------");
                list.forEach(c -> {
                    System.out.format("%-15s %-40s %-30s %-20s %-10s \n", 
                            c.getIdBuku(), c.getJudul(), c.getPengarang(),
                            c.getPenerbit(), c.getTahun());
                });
            };
        }
}
